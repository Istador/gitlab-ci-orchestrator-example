#!/bin/sh

cmd="$1"
lock=${2:-$CI_PROJECT_PATH_SLUG:$CI_COMMIT_REF_SLUG}
host="$GLCIO_HOST"
timeout="$GLCIO_TIMEOUT"
query="?project=$CI_PROJECT_ID&pipeline=$CI_PIPELINE_ID&id=$lock"

# host is required
if [ "$host" = "" ] ; then
  >&2  echo  "ERROR: env var \$GLCIO_HOST is empty"
  exit  1
fi

# optional timeout
if [ "$timeout" != "" ] ; then
  query="${query}&timeout=$timeout"
fi

# function http
http() {
  res=`curl  -sS  "$host/$cmd/$query"`
  if [ $? != 0 ] ; then
    >&2  echo  "ERROR: curl failed"
    exit  3
  fi
  if [ "$res" != "OK" ] ; then
    >&2  echo  "ERROR: fail to $cmd '$lock' - $res"
    exit  4
  fi
  echo  "OK: $cmd '$lock'"
  exit  0
}

# command
case "$cmd" in
  "lock"|"wait"|"unlock")
    http
    ;;
  *)
    >&2  echo  "ERROR: unknown command '$cmd'"
    >&2  echo  "  Usage: lock.sh <lock|wait|unlock> [id]"
    exit  2
    ;;
esac
